import './App.css';
import ToDoItem from './toDo/ToDoItem';
import todosData from './toDo/todosData';
import React, { useEffect, useState } from 'react';

function App () {

const [todoItems, setTodoItems] = useState(() => {
  const savedData = localStorage.getItem("tasks");
  if (savedData) {
    return JSON.parse(savedData);
  } else {
    return todosData;
  }
});

const [text, setText] = useState([]);

useEffect(() => {
  localStorage.setItem("tasks", JSON.stringify(todoItems));
   //localStorage.clear();
}, [todoItems]);

const handleChange = id => {
    const taskArr = [...todoItems];
    const index = taskArr.map(item => item.id).indexOf(id);
    taskArr[index].completed = !todoItems[index].completed;
    setTodoItems(taskArr);
    return taskArr;      
}
   
const addTask = task => { 
    const newTask = { 
      id : todoItems.length + 1,
      text : task,
      completed : false
    }
    let newTaskArray = [...todoItems, newTask];
    setTodoItems(newTaskArray);
    setText([]);
    return todoItems; 
  }

const deleteTask = id => {
  const taskArr = [...todoItems.filter(task => task.id !== id)];
  setTodoItems(taskArr);
  return taskArr;
}


const activeTasks = todoItems.filter(task => task.completed === false);
const completedTasks = todoItems.filter(task => task.completed === true);
const finalTasks = [...activeTasks, ...completedTasks].map(item => {
  return (
    <ToDoItem
      key = {item.id} 
      description = {item.text}
      completed = {item.completed}
      handleChange = {() => setTodoItems(handleChange(item.id))}
      deleteTask = {() => setTodoItems(deleteTask(item.id))}
    />
  );
  })
return (
  <div className='App'>
    <input type="text" value={text} onChange={(e) => setText(e.target.value)}/> 
    <button onClick={() => addTask(text)}>Add a new Task</button>
      {finalTasks}
  </div>
)
}

export default App;