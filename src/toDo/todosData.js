const todosData = [
    {
        id: 1,
        text: "Go to school",
        completed: false
    },
    {
        id: 2,
        text: "Clean my teeth",
        completed: false
    },
    {
        id: 3,
        text: "Cook a dish",
        completed: false
    },
    {
        id: 4,
        text: "Fill the car",
        completed: false
    },
    {
        id: 5,
        text: "Buy some milk",
        completed: false
    },
    {
        id: 6,
        text: "Walk with the dog",
        completed: false
    }
]

export default todosData;