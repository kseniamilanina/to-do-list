import React from 'react';
import classes from './ToDoItem.scss';

const ToDoItem = props => {
    const resolvedClass = {
        textDecoration : 'line-through'
    }

    return (
        <div className='todo-item'>
            <p className="description"
                style ={props.completed === true ? resolvedClass : {}} 
            >{props.description}</p>
            <input className="checkbox" type="checkbox" 
                onChange={props.handleChange} 
                defaultChecked={props.completed}
            />
            <button className='btn-delete' onClick={props.deleteTask}>Delete task</button>
           
        </div>
    )
}

export default ToDoItem;